// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order
// and log the results in the console as it was returned.

function sortCarModelsAlphabetically(inventory) {
    if(!Array.isArray(inventory)){
        return ("Inventory must be an array !");
    }
    if(inventory.length === 0){
        return ("Inventory is empty !");
    }
    
  let carModels = [];
  for (let index = 0; index < inventory.length; index++) {
    carModels.push(inventory[index].car_model);
  }

  for (let index1 = 0; index1 < carModels.length - 1; index1++) {
    for (let index2 = index1 + 1; index2 < carModels.length; index2++) {
      if (carModels[index1].toLowerCase() > carModels[index2].toLowerCase()) {
        let temp = carModels[index1];
        carModels[index1] = carModels[index2];
        carModels[index2] = temp;
      }
    }
  }
  return carModels;
}
module.exports = sortCarModelsAlphabetically;
