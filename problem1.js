// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function 
//that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
function findCarById(inventory, id) {
    if (Array.isArray(inventory) && typeof id === "number") {
      for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].id == id) {
          
          return (`Car ${inventory[index].id} is a ${inventory[index].car_year} ${inventory[index].car_make} ${inventory[index].car_model}`);
        }
      }
      return (`Car with id ${id} not found.`);
    } else {
      return [];
    }
  }
  module.exports = findCarById;