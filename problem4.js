//The accounting team needs all the years from every car on the lot. Execute a function
//that will return an array from the dealer data containing only the car years and log
//the result in the console as it was returned.

function getCarYears(inventory) {
  if (!Array.isArray(inventory)) {
    return "Inventory must be an array!";
  }
  if (inventory.length === 0) {
    return "Inventory is empty !";
  }
  const carYears = [];

  for (let index = 0; index < inventory.length; index++) {
    carYears.push(inventory[index].car_year);
  }
  return carYears;
}
module.exports = getCarYears;
