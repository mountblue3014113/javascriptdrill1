
function bmwAndAudiCars(inventory) {
    if (!Array.isArray(inventory)) {
      return "Inventory must be an array!";
    }
    if (inventory.length === 0) {
      return "Inventory is empty !";
    }
    const olderCars = [];
    let countOfBmwAndAudi = 0;
  
    for (let index = 0; index < inventory.length; index++) {
      if (inventory[index].car_make ==="BMW" || inventory[index].car_make ==="Audi") {
        olderCars.push(inventory[index]);
        countOfBmwAndAudi++;
      }
    }
    if(countOfBmwAndAudi === 0){
        return (`No BMW and Audi cars in the inventory !`);
    }else{
    return olderCars;
    }
  }
  module.exports = bmwAndAudiCars;
  