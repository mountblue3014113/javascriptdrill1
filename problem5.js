// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000.
// Using the array you just obtained from the previous problem, find out how many
// cars were made before the year 2000 and return the array of older cars and log its length.

function carsOlderThan2000(inventory) {
  if (!Array.isArray(inventory)) {
    return "Inventory must be an array!";
  }
  if (inventory.length === 0) {
    return "Inventory is empty !";
  }
  const olderCars = [];
  let countOfOlderCars = 0;

  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].car_year < 2000) {
      olderCars.push(inventory[index].car_year);
      countOfOlderCars++;
    }
  }
  console.log(`There are ${countOfOlderCars} cars made before the year 2000`);
  return olderCars;
}
module.exports = carsOlderThan2000;
