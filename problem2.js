// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory
// is?  Log the make and model into the console in the format of:
function findLastCar(inventory) {
  if (Array.isArray(inventory)) {
    const lastCar = inventory[inventory.length - 1];
    if (lastCar) {
      return `Last car is a ${lastCar.car_make} ${lastCar.car_model}`;
    }
    return `There no car in the inventory`;
  } else {
    return [];
  }
}
module.exports = findLastCar;
